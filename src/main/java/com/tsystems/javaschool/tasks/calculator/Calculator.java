package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;
import java.lang.String;
import java.lang.Math;

class PolishNotation {
    public String toReversePolish(String expression) {
        if (expression == null)
            return null;
        String res = "";
        int len = expression.length();
        Stack<Character> operator = new Stack<Character>();
        Stack<String> reversePolish = new Stack<String>();
        operator.push('#');
        for (int i = 0; i < len;) {
            while (i < len && expression.charAt(i) == ' ')
                i++;
            if (i == len)
                break;
            if (isNum(expression.charAt(i))) {
                String num = "";
                while (i < len && (isNum(expression.charAt(i)) ||  String.valueOf(expression.charAt(i)).equals(".")))
                    num += expression.charAt(i++);
                reversePolish.push(num);
            } else if (isOperator(expression.charAt(i))) {
                char op = expression.charAt(i);
                switch (op) {
                    case '(':
                        operator.push(op);
                        break;
                    case ')':
                        while (operator.peek() != '(')
                            reversePolish.push(Character.toString(operator.pop()));
                        operator.pop();
                        break;
                    case '+':
                    case '-':
                        if (operator.peek() == '(')
                            operator.push(op);
                        else {
                            while (operator.peek() != '#' && operator.peek() != '(')
                                reversePolish.push(Character.toString(operator.pop()));
                            operator.push(op);
                        }
                        break;
                    case '*':
                    case '/':
                        if (operator.peek() == '(')
                            operator.push(op);
                        else {
                            while (operator.peek() != '#' && operator.peek() != '+' &&
                                    operator.peek() != '-' && operator.peek() != '(')
                                reversePolish.push(Character.toString(operator.pop()));
                            operator.push(op);
                        }
                        break;
                }
                i++;
            }
        }
        while (operator.peek() != '#')
            reversePolish.push(Character.toString(operator.pop()));
        while (!reversePolish.isEmpty())
            res = res.length() == 0? reversePolish.pop() + res: reversePolish.pop() + " " + res;
        return res;
    }

    public boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == ')';
    }

    public boolean isNum(char c) {
        return c - '0' >= 0 && c - '0' <= 9;
    }
}



public class Calculator {


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.equals("")) {
            return null;
        }

        String[] statementToCheck = statement.split("");
        Stack<String> bracketsStack = new Stack<String>();
        int balance = 0;
        String prev = "";

        for(int i = 0; i < statementToCheck.length; i++){
            String currChar = statementToCheck[i];
            try {
                Double.parseDouble(currChar);
            } catch(NumberFormatException e){
                if (currChar.equals(prev)) {
                    return null;
                }
            }
            prev = currChar;
            if (currChar.equals(",")) {
                return null;
            }
            if (currChar.equals("(")) {
                bracketsStack.push("(");
                balance +=1;
            } else if (currChar.equals(")")) {
                balance -= 1;
                if (balance < 0) {
                    return null;
                }
                if(bracketsStack.empty()) {
                    return null;
                }
                bracketsStack.pop();
            }
        }
        if (!bracketsStack.isEmpty()) {
            return null;
        }

        PolishNotation polisNotation = new PolishNotation();
        String inPolishExpr = polisNotation.toReversePolish(statement);
        Stack<String> charStack = new Stack<String>();
        String[] polishExpr = inPolishExpr.split(" ");
        double res = 0;

        for (String currChar: polishExpr) {
            try {
                Double.parseDouble(String.valueOf(currChar));
                charStack.push(currChar);
            } catch(NumberFormatException e){
                String right = charStack.pop();
                String left = charStack.pop();

                if (currChar.equals("+")) {
                    res = Double.parseDouble(left) + Double.parseDouble(right);
                    charStack.push(String.valueOf(res));
                } else if (currChar.equals("-")) {
                    res = Double.parseDouble(left) - Double.parseDouble(right);
                    charStack.push(String.valueOf(res));
                } else if (currChar.equals("*")) {
                    res = Double.parseDouble(left) * Double.parseDouble(right);
                    charStack.push(String.valueOf(res));
                } else if (currChar.equals("/")) {
                    if (!((Float.parseFloat(right))==0)) {
                        res = Double.parseDouble(left) / Double.parseDouble(right);
                        charStack.push(String.valueOf(res));
                    } else {
                        return null;
                    }
                }
            }
        }

        String lastNumber = charStack.pop();
        Double lastNumberDouble = Double.parseDouble(lastNumber);
        if (Double.parseDouble(lastNumber) % 1 == 0) {
            return String.valueOf(Math.round(lastNumberDouble));
        } else {
            return String.valueOf(lastNumberDouble);
        }
    }
}
