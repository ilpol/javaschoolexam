package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        boolean isSubsequence = true;
        int rightIndex = 0;
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        for(int i = 0; i<x.size(); i++) {
            isSubsequence = false;
            while (rightIndex < y.size()) {
                if (x.get(i).equals(y.get(rightIndex))) {
                    rightIndex += 1;
                    isSubsequence = true;
                    break;
                } else {
                    rightIndex += 1;
                }
            }
            if (!isSubsequence && rightIndex==y.size() - 1) {
                return false;
            }
        }
        return isSubsequence;
    }
}
