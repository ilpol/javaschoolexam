package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        List<Integer> inputNumbersSort = inputNumbers;
        for (Integer num: inputNumbersSort) {
            if (num == null) {
                throw new CannotBuildPyramidException();
            }
        }
        if (inputNumbersSort.size() >=Integer.MAX_VALUE - 1) {
            throw new CannotBuildPyramidException();
        }
        try {
            Collections.sort(inputNumbersSort);
        } catch (Exception e){
            throw new CannotBuildPyramidException();
        }

        // find number of rows
        int rowN = 0;
        int curRowCount = 0;
        for (int num: inputNumbersSort) {
            if (curRowCount == rowN) {
                curRowCount = 0;
                rowN += 1;
            }
            curRowCount += 1;
        }

        // find number of columns
        int curM = 1;
        int curN = 1;
        int curK = 1;
        for (int n: inputNumbersSort) {
            if (curN == curM) {
                curK = curM;
                curM +=1;
                curN = 0;
            }
            curN +=1;
        }
        int colN = curK + curK - 1;

        int[][] res = new int[rowN][colN];
        int baseIndex = 0;
        int curMaxInRow = 1;
        int curPosInRow = (int)Math. ceil(colN / 2);

        for (int i = 0; i< rowN; i++) {
            for (int j = 0; j < colN; j++) {
                res[i][j] = 0;
            }

            int j = curPosInRow;
            int curOffsetCounter = 1;
            int curOffset = 0;

            while (curOffsetCounter <= curMaxInRow) {
                if (j + curOffset > colN || i > rowN || baseIndex > inputNumbersSort.size()) {
                    throw new CannotBuildPyramidException();
                }
                if (j < 0 || i < 0) {
                    throw new CannotBuildPyramidException();
                }
                res[i][j + curOffset] = inputNumbersSort.get(baseIndex);
                curOffset += 1;
                baseIndex += 1;
                curOffsetCounter += 1;
                j +=1;
            }
            curPosInRow -= 1;
            curMaxInRow += 1;
        }
        return res;
    }
}
